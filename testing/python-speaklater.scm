;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2020 jgart <jgart@dismail.de>
;;;
;;; This file is part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (testing python-speaklater)
  #:use-module (guix download)
  #:use-module (guix build-system python)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages))


(define-public python-speaklater
  (package
    (name "python-speaklater")
    (version "1.3")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "speaklater" version))
        (sha256
          (base32
            "1ab5dbfzzgz6cnz4xlwx79gz83id4bhiw67k1cgqrlzfs0va7zjr"))))
    (build-system python-build-system)
    (home-page
      "http://github.com/mitsuhiko/speaklater")
    (synopsis
      "implements a lazy string for python useful for use with gettext")
    (description
      "implements a lazy string for python useful for use with gettext")
    (license license:bsd-3)))

